// const gulp = require("gulp");
// const clean = require("gulp-clean");
// const ts = require("gulp-typescript");

// const tsProject = ts.createProject('tsconfig.json');

// gulp.task("static",()=>{
//     return gulp
//         .src(['src/**/*.json'])
//         .pipe(gulp.dest('dist'));
// });

// gulp.task('clean',()=>{
//     return gulp
//         .src('dist')
//         .pipe(clean());
// })

// gulp.task('build',['clean','static','scripts']);


const {series, src, dest, watch} = require('gulp');
const clean = require('gulp-clean')
const ts = require('gulp-typescript');

const tsProject = ts.createProject('tsconfig.json');

function scripts(){
    const tsResult = tsProject.src()
                    .pipe(tsProject());
    return tsResult.js
            .pipe(dest('dist'));
}
function static(cb){
    return src(['src/**/*.json'])
        .pipe(dest('dist'));
}
function cleanStatic(cb){
    return src('dist')
        .pipe(clean());
}

exports.watch = watch('src/**/*.ts',series(cleanStatic,static,scripts));

exports.build = series(cleanStatic,static,scripts);